import { Component } from '@angular/core';

import { HeaderBaseComponent } from '@ng-hackers/features';

@Component({
  selector: 'ng-hackers-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent extends HeaderBaseComponent {}
