import { Component } from '@angular/core';

import { BaseComponent } from '@ng-hackers/core';

@Component({
  selector: 'ng-hackers-home',
  templateUrl: 'home.component.html'
})
export class HomeComponent extends BaseComponent {}
