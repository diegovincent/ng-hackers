import { Component } from '@angular/core';

// xplat
import { AppBaseComponent } from '@ng-hackers/web';

@Component({
  selector: 'ng-hackers-root',
  templateUrl: './app.component.html'
})
export class AppComponent extends AppBaseComponent {
  constructor() {
    super();
  }
}
