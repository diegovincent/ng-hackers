import { NgModule } from '@angular/core';

// xplat
import { NgHackersCoreModule } from '@ng-hackers/web';

@NgModule({
  imports: [NgHackersCoreModule]
})
export class CoreModule {}
