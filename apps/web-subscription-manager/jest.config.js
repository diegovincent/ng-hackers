module.exports = {
  name: 'web-subscription-manager',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/web-subscription-manager',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
